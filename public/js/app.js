var app = angular.module('eldritchApp', ['ngRoute', 'ui.bootstrap']);

app.config(function($routeProvider, $locationProvider) {
  $routeProvider
    .when('/', {
      templateUrl: 'views/board.html',
      controller: 'GameController'
    });

  $locationProvider.html5Mode(false);
});
