app.factory('util', function () {
  function shuffleStack(stack, n) {
    if (stack === undefined) { return null; }
    var s = _.clone(stack, true);
    var i, j, k;
    var temp;

    if (n === undefined) { n = 1; }

    // Shuffle the stack 'n' times.
    for (i = 0; i < n; i++) {
      for (j = 0; j < s.length; j++) {
	k = Math.floor(Math.random() * s.length);
	temp = s[j];
	s[j] = s[k];
	s[k] = temp;
      }
    }

    return s;
  }

  function referenceCard(game) {
    var ref = {};
    if (game === undefined) { return ref; }
    ref = {spawn_gates: 1, spawn_clues: 1, monster_surge: 1};
    return ref;
  }

  // returns an array of results - does NOT return sum of all rolls
  function rollDice(n, sides) {
    if (n === undefined) { n = 1; }
    if (sides === undefined) { sides = 6; }

    var i = 0;
    var res = [];
    for (i=0; i < n; i++) {
      res.push(Math.floor(Math.random() * sides) + 1);
    }

    return res;
  }

  function testRoll(rolls) {
//    var roll = rollDice(amount);
    var pass = false;
    var successes = 0;
    var res = {successes: successes,
	       pass: pass,
	       rolls: rolls};

    _.each(rolls, function (result) {
      if (result === 5 || result === 6) { successes += 1;}
    });
    if (successes > 0) { pass = true; }

    res.successes = successes;
    res.pass = pass;
    return res;
  }

  function reroll(rolls, amount) {
    var r = _.sortBy(rolls, function (num) {
      return num;
    });
    var i = 0;
    var j;
    for (i = 0; i < amount; i++) {
      j = rollDice();
      r[i] = j[0];
    }
    return r;
  }

  function sortMythosCardsByColor(cards) {
    var c = _.clone(cards, true);
    var res = {green: [],
               blue: [],
               yellow: []};
    _.each(c, function (mythos_card) {
      var color = mythos_card.color.toLowerCase();
      res[color].push(mythos_card);
    });
    return res;
  }

  function sortLocationCardsByContinent(cards) {
    var c = _.clone(cards, true);
    var res = {general: [],
               america: [],
               asia_australia: [],
               europe: []};
    _.each(c, function (location_card) {
      var cont = location_card.continent.toLowerCase();
      res[cont].push(location_card);
    });
    return res;
  }

  function createMythosDeck(mythos_deck, mythos_cards) {
    var stage_one_deck = [],
        stage_two_deck = [],
        stage_three_deck = [],
        card,
        i,
        temp_deck = [];

    _.each(mythos_deck.stage_one, function (amount, color) {
      for (i = 0; i < amount; i++) {
	card = _.last(mythos_cards[color]);
	if (card) {
	  stage_one_deck.push(card);
	}
      }
    });
    _.each(mythos_deck.stage_two, function (amount, color) {
      for (i = 0; i < amount; i++) {
	card = _.last(mythos_cards[color]);
	if (card) {
	  stage_two_deck.push(card);
	}
      }
    });
    _.each(mythos_deck.stage_three, function (amount, color) {
      for (i = 0; i < amount; i++) {
	card = _.last(mythos_cards[color]);
	if (card) {
	  stage_three_deck.push(card);
	}
      }
    });
    
    stage_one_deck = shuffleStack(stage_one_deck);
    stage_two_deck = shuffleStack(stage_two_deck);
    stage_three_deck = shuffleStack(stage_three_deck);

    temp_deck.push(stage_one_deck);
    temp_deck.push(stage_two_deck);
    temp_deck.push(stage_three_deck);

    return temp_deck;
  }

  function parseAssetModifiers(stack) {
    var modifiers = {strength_modifier: 0};
    _.each(stack, function (card) {
      if (card.effects) {
	if (card.effects.during_encounter) {
	  if (card.effects.during_encounter.combat) {
	    if (card.effects.during_encounter.combat.strength_modifier) {
	      modifiers.strength_modifier = card.effects.during_encounter.combat.strength_modifier;
	    }
	    if (card.effects.during_encounter.combat.reduce_damage_taken) {
	      modifiers.damage = -(card.effects.during_encounter.combat.reduce_damage_taken.amount);
	      modifiers.minimum_reduce = card.effects.during_encounter.combat.reduce_damage_taken.minimum;
	    }
	  }
	}
      }
    });
    return modifiers;
  }

  function parseAsset(card) {
    var m;
    _.each(card.effects, function (effect) {
      m = effect.match(/(Gain) (\+\d*) (Strength|Will) during (Combat|Location) (Encounter)s/i);
      if (m) {
        
      }
    });
  }

  function parseConditionCards(stack, restrictions) {
    if (restrictions === undefined) { restrictions = {cannot: [], encounters: []}; }
    _.each(stack, function (card) {
      if (card.restrictions) {
	if (card.restrictions.cannot){
	  _.each(card.restrictions.cannot, function (cannot) {
	    restrictions.cannot.push(cannot);
	  });
	}
      }
    });
    return restrictions;
  }

  function playerMove(player, connection) {
    var p = _.clone(player, true);
    _.times(connection.distance, function () {
      p.actions_taken.moves += 1;
      p.actions_taken.travel = true;
      if (p.actions_taken.moves < 2) {
	p.actions_taken.count += 1;
      }
    });

    if (p.actions_taken.moves > 1 && connection.ticket_type) {
      switch (connection.ticket_type) {
      case "ship":
        if (p.travel_tickets.ship > 0) p.travel_tickets.ship -= 1;
        break;
      case "train":
        if (p.travel_tickets.train > 0) p.travel_tickets.train -= 1;
        break;
      default:
        break;
      }
    }

    return p;
  }

  function resetActionsTaken() {
    return {
      moves: 0,
      travel: false,
      rest: false,
      trade: false,
      prepare: false,
      acquire_assets: false,
      component_action: false,
      combat: [],
      count: 0
    };
  }

  function createClueTokens(locations) {
    var tokens = [];
    var token = {};
    _.each(locations, function (location) {
      token = {location: location.name};
      tokens.push(token);
    });
    return tokens;
  }

  function formSentence(effect) {
    var str = "";
    var t,
        from = "",
        misc = "";
    _.each(effect, function (effect, effect_name) {
      if (effect_name === "test") {
        var modifier = "";
        if (effect.modifier && (effect.modifier > 0 || effect.modifier < 0)) modifier = effect.modifier;
        str += "Test " + effect.skill + " " + modifier;
      } else {
        if (effect.type) {
          t = effect.type;
        } else if (effect.attr) {
          t = effect.attr;
        } else if (effect.token) {
          t = effect.token;
        }
        if (effect.from) {
          from = " from " + effect.from;
        }
        if (effect.random) {
          misc += " at random";
        }
        str += _.capitalize(effect_name) + " " + effect.amount + " " + t + from + misc;
      }
    });
    return str;
  }  

  return {
    shuffleStack: shuffleStack,
    referenceCard: referenceCard,
    rollDice: rollDice,
    testRoll: testRoll,
    reroll: reroll,
    sortLocationCardsByContinent: sortLocationCardsByContinent,
    sortMythosCardsByColor: sortMythosCardsByColor,
    createMythosDeck: createMythosDeck,
    parseAssetModifiers: parseAssetModifiers,
    parseConditionCards: parseConditionCards,
    playerMove: playerMove,
    resetActionsTaken: resetActionsTaken,
    createClueTokens: createClueTokens,
    formSentence: formSentence
  };
});
