app.factory('database', ['$http', function ($http) {
  function getInvestigatorsFromDB() {
    return $http.get('/api/investigators')
      .then(function (res) {
	return res;
      });
  }

  function getAncientOnesFromDB() {
    return $http.get('/api/ancient_ones')
      .then(function (res) {
	return res;
      });
  }

  function getMythosCardsFromDB() {
    return $http.get('/api/mythos_cards')
      .then(function (res) {
	return res;
      });
  }

  function getAssetCardsFromDB() {
    return $http.get('/api/asset_cards')
      .then(function (res) {
	return res;
      });
  }

  function getMapLocationsFromDB() {
    return $http.get('/api/map_locations')
      .then(function (res) {
	return res;
      });
  }

  function getGateTokensFromDB() {
    return $http.get('/api/gate_tokens')
      .then(function (res) {
	return res;
      });
  }

  function getMonsterTokensFromDB() {
    return $http.get('/api/monster_tokens')
      .success(function (data, status, header, config) {
	return data;
      });
  }

  function getOtherWorldCardsFromDB() {
    return $http.get('/api/other_world_cards')
      .success(function (data, status, header, config) {
	return data;
      });
  }

  function getLocationCardsFromDB() {
    return $http.get('/api/location_cards')
      .success(function (data, status, header, config) {
        return data;
      });
  }

  function getResearchCardsFromDB() {
    return $http.get('/api/research_cards')
      .success(function (data, status, header, config) {
        return data;
      });
  }

  function getConditionCardsFromDB() {
    return $http.get('/api/condition_cards')
      .success(function (data, status, header, config) {
        return data;
      });
  }

  function getExpeditionCardsFromDB() {
    return $http.get('/api/expedition_cards')
      .success(function (data, status, header, config) {
        return data;
      });
  }

  return {
    getInvestigatorsFromDB: getInvestigatorsFromDB,
    getAncientOnesFromDB: getAncientOnesFromDB,
    getMythosCardsFromDB: getMythosCardsFromDB,
    getAssetCardsFromDB: getAssetCardsFromDB,
    getMapLocationsFromDB: getMapLocationsFromDB,
    getGateTokensFromDB: getGateTokensFromDB,
    getMonsterTokensFromDB: getMonsterTokensFromDB,
    getOtherWorldCardsFromDB: getOtherWorldCardsFromDB,
    getLocationCardsFromDB: getLocationCardsFromDB,
    getResearchCardsFromDB: getResearchCardsFromDB,
    getConditionCardsFromDB: getConditionCardsFromDB,
    getExpeditionCardsFromDB: getExpeditionCardsFromDB
  };
}]);
