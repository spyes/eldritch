app.factory('mommy', function () { 
  function canPlayerTakeCardFromReserve(player) {
    if (player === undefined) { return false; }
    var can = false;
    if (player.investigator.current_location.monsters.length === 0 && player.actions_taken.count < 2 && !player.actions_taken.acquire_assets) { can = true; }
    return can;
  } 

  function canPlayerMove(player, connection) {
    if (player === undefined) { return false; }
    var can = false;
    if (player.actions_taken.count < 2 &&
	(player.actions_taken.moves === 0 ||
	 (player.actions_taken.moves === 1 && connection.distance < 2))) {
      can = true;
    }
    return can;	    
  }

  function canPlayerRest(player, phase) {
    if (player === undefined || phase === undefined) { return false; }
    var can = true;
    if ((player.investigator.health === player.investigator.max_health &&
	 player.investigator.sanity === player.investigator.max_sanity) ||
	player.actions_taken.rest || 
	player.actions_taken.count > 1 || 
	player.investigator.current_location.monsters.length > 0 ||
	phase !== "action") {
      can = false;
    }
    return can;
  }

  function canPlayerCombatMonster(player, monster) {
    var can = true;
    if (_.includes(player.actions_taken.combat, monster)) {
      can = false;
    }
    return can;
  }

  return {
    canPlayerTakeCardFromReserve: canPlayerTakeCardFromReserve,
    canPlayerMove: canPlayerMove,
    canPlayerRest: canPlayerRest,
    canPlayerCombatMonster: canPlayerCombatMonster
  };
});
