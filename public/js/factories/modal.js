app.factory('modal', ['$modal', function ($modal) {
  function openGenericModal(options) {
    var modalInstance = $modal.open(options);
    return modalInstance;
  }

  function open(msg, template, controller) {
    if (template === undefined) { 
      template = "<div style='padding: 10px; line-height: 25px'>" + msg + "</div>";
    }
    var options = {};
    options.template = template;
    if (controller !== undefined) options.controller = controller;

    return openGenericModal(options);
  }

  function OrModal(options, callback) {
    var modal_options = {
      templateUrl: "../../views/or_modal.html",
      controller: "OrModalController",
      backdrop: "static",
      resolve: {
	options: function () { return options; }
      }
    };
    var modalInstance = openGenericModal(modal_options);
    modalInstance.result.then(function (selection) {
      callback(selection);
    });
  }

  function SpendCluesToRerollModal(clues) {
    var modal_options = {
      templateUrl: "../../views/spend_clues_modal.html",
      controller: "SpendCluesModalController",
      backdrop: "static",
      resolve: {
	clues: function () { return clues; }
      }
    };
  }

  return {
    open: open,
    OrModal: OrModal,
    SpendCluesToRerollModal: SpendCluesToRerollModal
  };
}]);